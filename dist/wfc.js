"use strict";
/*
Research started: December 1, 2018
Project started: December 4, 2018 at 10:21PM

Wave Function Collapse Algorithm
======================================
OK - Generate patterns from source image
OK - Generate pattern overlaps
Loop until image complete:
    Observe
        Find the grid cell with the lowest entropy
        Pick a random weight pattern for that cell
    Propagate
Render the final image
*/
class PatternGenerator {
    static PalettizeImage(srcData) {
        const pixelCount = srcData.width * srcData.height * 4;
        const data = srcData.data;
        const output = new IndexedColorImage(srcData.width, srcData.height);
        const palette = [];
        const colorMap = new Map();
        let curColor = 0;
        //Go through each pixel
        for (let i = 0; i < pixelCount; i += 4) {
            const color = data.slice(i, i + 4);
            const colorKey = color.join("-");
            //Add it to the palette
            if (!colorMap.has(colorKey)) {
                palette.push(Array.from(color));
                colorMap.set(colorKey, curColor++);
            }
            //Set pixel on palette image
            const colorIndex = colorMap.get(colorKey);
            output.SetColorLinear(i / 4, colorIndex);
        }
        return { palette: palette, indexedImage: output };
    }
    //Returns all of the unique patterns and their frequencies as a Map
    static GeneratePatterns(indexedSrcData, N, paletteSize) {
        const patternFrequencies = new Map();
        //Find as many unique patterns as possible in the source image.
        for (let y = 0; y < indexedSrcData.height; y++) {
            for (let x = 0; x < indexedSrcData.width; x++) {
                //Get the pattern in this location.
                const patterns = new Array(8);
                patterns[0] = Pattern.GetPatternFromIndexedImage(indexedSrcData, x, y, N);
                //Transform the pattern by reflecting and rotating it in a variety of ways.
                patterns[1] = patterns[0].ReflectVertical();
                patterns[2] = patterns[0].Rotate();
                patterns[3] = patterns[2].ReflectVertical();
                patterns[4] = patterns[2].Rotate();
                patterns[5] = patterns[4].ReflectVertical();
                patterns[6] = patterns[4].Rotate();
                patterns[7] = patterns[6].ReflectVertical();
                //Track the unique patterns and count how much they occur.
                //This will be used later during the observe step.
                for (let i = 0; i < patterns.length; i++) {
                    const patternID = patterns[i].GetPatternID(paletteSize);
                    //Testing
                    const reconstructedPattern = Pattern.FromPatternID(patternID, paletteSize, N);
                    if (!this.CanOverlap(patterns[i], reconstructedPattern, 0, 0))
                        debugger;
                    if (patternFrequencies.has(patternID)) {
                        patternFrequencies.set(patternID, patternFrequencies.get(patternID) + 1);
                    }
                    else {
                        patternFrequencies.set(patternID, 1);
                    }
                }
            }
        }
        return { patternFrequencies, patternCount: patternFrequencies.size };
    }
    static GeneratePatternOverlapTable(patternIDs, N, paletteSize) {
        const patterns = patternIDs.map(id => Pattern.FromPatternID(id, paletteSize, N));
        const startLoop = -N + 1, endLoop = N;
        //Give each pattern a table to check what can overlap it.
        const propagationTable = new Array(patternIDs.length);
        for (let i = 0; i < propagationTable.length; i++) {
            propagationTable[i] = new PatternOverlapTable(N);
        }
        //Go through every pattern and check which patterns can overlap it.
        for (let startPattern = 0; startPattern < patterns.length; startPattern++) {
            for (let dy = startLoop; dy < endLoop; dy++) {
                for (let dx = startLoop; dx < endLoop; dx++) {
                    //console.log(`Current Offset: (${dx}, ${dy})`)
                    //Can this pattern overlap the current pattern?
                    const overlapTable = propagationTable[startPattern];
                    for (let compPattern = 0; compPattern < patterns.length; compPattern++) {
                        //console.log(`Comparing Pattern ${startPattern} & ${compPattern} at (${dx}, ${dy})`)
                        if (this.CanOverlap(patterns[startPattern], patterns[compPattern], dx, dy)) {
                            //console.log(`OVERLAP: Pattern ${startPattern} & ${compPattern} at (${dx}, ${dy})`)
                            overlapTable.MarkOverlap(dx, dy, compPattern);
                        }
                    }
                }
            }
        }
        return propagationTable;
    }
    static CanOverlap(curPattern, overlappingPattern, dx, dy) {
        //Sanity check
        if (curPattern.width !== curPattern.height ||
            curPattern.width !== overlappingPattern.width ||
            curPattern.width !== overlappingPattern.height) {
            throw new Error("Patterns do not match");
        }
        //Find the overlap area
        const N = curPattern.width;
        const ymin = dy < 0 ? 0 : dy;
        const ymax = dy < 0 ? dy + N : N;
        const xmin = dx < 0 ? 0 : dx;
        const xmax = dx < 0 ? dx + N : N;
        //Get the over lap of each pattern
        for (var y = ymin; y < ymax; y++) {
            for (var x = xmin; x < xmax; x++) {
                if (curPattern.GetColorLinear(x + N * y) !== overlappingPattern.GetColorLinear(x - dx + N * (y - dy))) {
                    return false;
                }
            }
        }
        return true;
    }
}
class PatternOverlapTable {
    constructor(N) {
        //The number of possible offsets is: (2 (N-1) + 1)^2
        this.patternTable = new Array(Math.pow((2 * (N - 1) + 1), 2));
        for (let i = 0; i < this.patternTable.length; i++) {
            this.patternTable[i] = new Set();
        }
        this.N = N;
    }
    MarkOverlap(dx, dy, patternNumber) {
        const overlapSet = this.patternTable[this.GetLinearIndex(dx, dy)];
        overlapSet.add(patternNumber);
    }
    CanOverlap(dx, dy, patternNumber) {
        return this.patternTable[this.GetLinearIndex(dx, dy)].has(patternNumber);
    }
    GetAllOverlaps(dx, dy) {
        return this.patternTable[this.GetLinearIndex(dx, dy)];
    }
    testFunction() {
        let N = 3;
        const startLoop = -N + 1, endLoop = N;
        let offsets = [];
        for (let dy = startLoop; dy < endLoop; dy++) {
            for (let dx = startLoop; dx < endLoop; dx++) {
                offsets.push([dx, dy]);
            }
        }
        //return [[-1,-1],[0,-1],[1,-1],[-1,0],[0,0],[1,0],[-1,1],[0,1],[1,1],].map(x => this.GetLinearIndex(x[0],x[1]))
        //console.log(offsets);
        return offsets.map(x => {
            const dx = x[0];
            const dy = x[1];
            return (dx + N - 1) + (dy + N - 1) * ((2 * N) - 1); //(2 * (N - 1));
        });
    }
    GetLinearIndex(dx, dy) {
        return (dx + this.N - 1) + (dy + this.N - 1) * ((2 * this.N) - 1);
        //return dx + (this.N) + (dy + this.N);// * (this.N * this.N);// + ((dy + this.N - 1) * (this.N * this.N - 1));
        //return (dy + (this.N - 1) * this.N) + dx + (this.N - 1); //dy * this.N + dx;
    }
}
class IndexedColorImage {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.pixels = new Array(width * height).fill(0);
    }
    SetColor(x, y, colorIndex) {
        this.SetColorLinear(this.GetLinearIndex(x, y), colorIndex);
    }
    GetColor(x, y) {
        return this.GetColorLinear(this.GetLinearIndex(x, y));
    }
    SetColorLinear(i, colorIndex) {
        this.BoundsCheck(i);
        this.pixels[i] = colorIndex;
    }
    GetColorLinear(i) {
        this.BoundsCheck(i);
        return this.pixels[i];
    }
    GetLinearIndex(x, y) {
        return y * this.width + x;
    }
    BoundsCheck(i) {
        if (i < 0 || i >= this.pixels.length)
            throw new Error("Index Out of Bounds");
    }
}
class Pattern extends IndexedColorImage {
    constructor(n) {
        super(n, n);
    }
    GetPatternID(paletteSize) {
        //Treat the pattern like a number of base <number of colors>
        let power = 1, outputID = 0;
        for (let i = this.pixels.length - 1; i >= 0; i--) {
            outputID += this.GetColorLinear(i) * power;
            power *= paletteSize;
        }
        return outputID;
    }
    Transform(mapFunction) {
        const output = new Pattern(this.width);
        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                output.SetColor(x, y, this.GetColorLinear(mapFunction(x, y)));
            }
        }
        return output;
    }
    Rotate() {
        return this.Transform((x, y) => this.GetLinearIndex(this.width - 1 - y, x));
    }
    ReflectVertical() {
        return this.Transform((x, y) => this.GetLinearIndex(this.width - 1 - x, y));
    }
    static GetPatternFromIndexedImage(srcImage, x, y, N) {
        const pattern = new Pattern(N);
        for (let dy = 0; dy < N; dy++) {
            for (let dx = 0; dx < N; dx++) {
                //pattern.SetColor((x + dx) % srcImage.width, (y + dy) % srcImage.height, srcImage.GetColor(x, y));
                pattern.SetColor(dx, dy, srcImage.GetColor((x + dx) % srcImage.width, (y + dy) % srcImage.height));
            }
        }
        return pattern;
    }
    static FromPatternID(patternID, paletteSize, N) {
        const output = new Pattern(N);
        let power = Math.pow(paletteSize, (N * N) - 1);
        for (let i = 0; i < output.pixels.length; i++) {
            let color = Math.floor(patternID / power);
            output.SetColorLinear(i, color);
            patternID -= power * color;
            power /= paletteSize;
        }
        return output;
    }
}
class Solver {
    constructor(outWidth, outHeight, N, patternTable, palette, overlapTables, rng) {
        //Output Dimensions
        this.width = outWidth;
        this.height = outHeight;
        this.complete = false;
        //Pattern Data
        this.N = N;
        this.patternIDs = [...patternTable.patternFrequencies.keys()];
        this.patternFreqs = [...patternTable.patternFrequencies.values()];
        this.patternPad = new Array(this.patternFreqs.length).fill(0);
        //this.overlapTable = overlapTable;
        //Get the palette data
        this.palette = palette;
        this.paletteSize = this.palette.length;
        //Set up the grid
        this.resultGrid = new Array(this.width);
        for (let x = 0; x < this.resultGrid.length; x++) {
            this.resultGrid[x] = new Array(this.height);
            for (let y = 0; y < this.resultGrid[x].length; y++) {
                this.resultGrid[x][y] = new Array(this.patternIDs.length);
                this.resultGrid[x][y].fill(true);
            }
        }
        //Random Number Generator for numbers [0,1). Should be seedable for consistency.
        this.rng = rng;
        //Has this solver been observed at least once?
        this.hasBeenObservedBefore = false;
        //Update Queue used by the propagation stage
        this.updateQueue = [];
        //The precalculated pattern overlap tables used by the propagation stage
        this.overlapTables = overlapTables;
    }
    Observe() {
        //Find the cell most likely to be collapsed.
        const [x, y] = (this.hasBeenObservedBefore) ? this.FindLowestEntropy() : this.GetRandomCell();
        //If the solver hasn't been observed, all cells have the same entropy.
        //It will probably land on the first one, so we pick one randomly.
        this.hasBeenObservedBefore = true;
        //No square was found. Observation is complete.
        if (x === -1 || y === -1) {
            this.complete = true;
            return true;
        }
        //Randomly select a pattern for this cell
        const cell = this.resultGrid[x][y];
        this.CollapseCellPattern(cell);
        //Mark this cell as changed for the propagation stage
        this.updateQueue.push([x, y]);
        return false;
    }
    Propagate() {
        const changedCells = [];
        const startLoop = -this.N + 1, endLoop = this.N;
        while (this.updateQueue.length !== 0) {
            const [x, y] = this.updateQueue.pop();
            const cell = this.resultGrid[x][y];
            //Update the neighboring cells
            for (let dx = startLoop; dx < endLoop; dx++) {
                for (let dy = startLoop; dy < endLoop; dy++) {
                    //This is the current cell. No need to look here!
                    if (dx === 0 && dy === 0)
                        continue;
                    //Wrap this selection across the grid
                    let { sx, sy } = this.wrapGridCoords(x, dx, y, dy);
                    //Get the adjacent cell
                    const adjacentCell = this.resultGrid[sx][sy];
                    //Go through the adjacent cell's possible patterns
                    for (let i = 0; i < adjacentCell.length; i++) {
                        //This pattern is invalid. No need to check.
                        if (!adjacentCell[i])
                            continue;
                        //Get this pattern's overlap table
                        const overlapTable = this.overlapTables[i];
                        //Go through the current cell's patterns.
                        //Can this pattern overlap any of the current cell's patterns? If so, it stays.
                        let canOverlap = false;
                        for (let i = 0; i < cell.length && !canOverlap; i++) {
                            canOverlap = cell[i] && overlapTable.CanOverlap(-dx, -dy, i);
                        }
                        //This pattern cannot be used next to the current cell.
                        //Remove the pattern. Mark for propagation update.
                        if (!canOverlap) {
                            adjacentCell[i] = false;
                            changedCells.push([sx, sy]);
                        }
                    }
                    //Safety Check: Has this cell become a contradiction?
                    if (this.GetSumOfCellWeight(adjacentCell) === 0)
                        throw new Error("Contradiction during propagation!");
                }
            }
        }
        //Signal if nothing changed in the propagation stage
        this.updateQueue = changedCells;
        return changedCells.length !== 0;
    }
    wrapGridCoords(x, dx, y, dy) {
        let sx = x + dx;
        let sy = y + dy;
        if (sx < 0)
            sx = (sx % this.width) + this.width;
        if (sy < 0)
            sy = (sy % this.height) + this.height;
        if (sx >= this.width)
            sx %= this.width;
        if (sy >= this.height)
            sy %= this.height;
        return { sx, sy };
    }
    GetCellPatternCount(cell) {
        let sum = 0;
        for (let i = 0; i < cell.length; i++) {
            if (cell[i])
                sum++;
        }
        return sum;
    }
    GetCellPattern(cell) {
        for (let i = 0; i < cell.length; i++) {
            if (cell[i])
                return i;
        }
        return -1;
    }
    GetRandomCell() {
        return [Math.floor(this.rng() * this.width), Math.floor(this.rng() * this.height)];
    }
    FindLowestEntropy() {
        let minEntropy = 1000;
        let minX = -1, minY = -1;
        for (let x = 0; x < this.resultGrid.length; x++) {
            for (let y = 0; y < this.resultGrid[x].length; y++) {
                const cell = this.resultGrid[x][y];
                const cellTotalWeight = this.GetSumOfCellWeight(cell);
                //If no valid patterns exist here, that's bad.
                if (cellTotalWeight === 0)
                    throw new Error(`Encountered a contradiction at (${x}, ${y})!`);
                //Get the cell's entropy
                let entropy = this.GetCellEntropy(cell, cellTotalWeight);
                //Here's some random noise to break ties
                const noise = 0.000001 * this.rng();
                //Get the minimum entropy and its cell
                if (entropy > 0 && entropy + noise < minEntropy) {
                    minEntropy = entropy;
                    minX = x;
                    minY = y;
                }
            }
        }
        return [minX, minY];
    }
    GetSumOfCellWeight(cell) {
        let sum = 0;
        for (let i = 0; i < cell.length; i++) {
            this.patternPad[i] = cell[i] ? this.patternFreqs[i] : 0;
            sum += this.patternPad[i];
        }
        return sum;
    }
    GetCellEntropy(cell, totalWeight) {
        let entropy = 0;
        for (let i = 0; i < cell.length; i++) {
            if (cell[i]) {
                //Normalize the frequencies into probabilities
                const probability = this.patternFreqs[i] / totalWeight;
                //Calculate the entropy
                entropy += -probability * Math.log(probability);
            }
        }
        return entropy;
    }
    SelectRandomPattern(cell) {
        //A basic linear weighted search
        let randomSelection = this.rng() * this.GetSumOfCellWeight(cell);
        for (let i = 0; i < this.patternIDs.length; i++) {
            if (cell[i]) {
                randomSelection -= this.patternFreqs[i];
                if (randomSelection <= 0)
                    return i;
            }
        }
        //Somehow that failed...
        throw new Error("Failed Random Selection.");
    }
    CollapseCellPattern(cell) {
        //Given this cell's options, pick a random pattern weighted by it's frequency
        const patternIndex = this.SelectRandomPattern(cell);
        //Collapse this cell to this particular pattern
        cell.fill(false);
        cell[patternIndex] = true;
    }
    ClearCells() {
        this.hasBeenObservedBefore = false;
        this.complete = false;
        for (let x = 0; x < this.resultGrid.length; x++) {
            for (let y = 0; y < this.resultGrid[x].length; y++) {
                this.resultGrid[x][y].fill(true);
            }
        }
    }
    ExplodeCell(x, y, radius) {
        for (let dx = -radius; dx < radius; dx++) {
            for (let dy = -radius; dy < radius; dy++) {
                const distance = Math.sqrt(dx * dx + dy * dy);
                if (distance > radius)
                    continue;
                const { sx, sy } = this.wrapGridCoords(x, dx, y, dy);
                this.resultGrid[sx][sy].fill(true);
            }
        }
    }
}
//Shamelessly stolen from user: Antti Kissaniemi
//https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
class SeededRandom {
    constructor(seed = 123456789) {
        this.m_w = 123456789;
        this.m_z = 987654321;
        this.mask = 0xffffffff;
        //this.Seed(seed);
    }
    // Takes any integer
    Seed(i) {
        this.m_w = i;
        this.m_z = 987654321;
    }
    // Returns number between 0 (inclusive) and 1.0 (exclusive),
    // just like Math.random().
    Random() {
        this.m_z = (36969 * (this.m_z & 65535) + (this.m_z >> 16)) & this.mask;
        this.m_w = (18000 * (this.m_w & 65535) + (this.m_w >> 16)) & this.mask;
        var result = ((this.m_z << 16) + this.m_w) & this.mask;
        result /= 4294967296;
        return result + 0.5;
    }
}
function generateRedMaze() {
    const imgData = new ImageData(4, 4);
    const colors = [[255, 255, 255, 255], [0, 0, 0, 255], [255, 0, 0, 255], [0, 255, 0, 255]];
    const palette = [
        0, 0, 0, 0,
        0, 1, 1, 1,
        0, 1, 2, 1,
        0, 1, 1, 1,
    ];
    for (let i = 0; i < (4 * 4) * 4; i += 4) {
        const colorIndex = i / 4;
        imgData.data[i + 0] = colors[palette[colorIndex]][0];
        imgData.data[i + 1] = colors[palette[colorIndex]][1];
        imgData.data[i + 2] = colors[palette[colorIndex]][2];
        imgData.data[i + 3] = colors[palette[colorIndex]][3];
    }
    return imgData;
}
function ShowPatterns(patternTable, palette, N, overlapPrinterData) {
    //Title and description
    const header = createElement("h2", `Calculated ${patternTable.patternFrequencies.size} Patterns from Source Image`);
    const text = createElement("p", "Click on a pattern to see its overlap table. CTRL + Click to close the table.");
    header.style.margin = text.style.margin = "0";
    let i = 0;
    for (let [patternID, freq] of patternTable.patternFrequencies.entries()) {
        const header = createElement("h3", `${i++}. Pattern #${patternID} (Freq: ${freq})`);
        header.style.margin = "5px auto";
        const imgData = Pattern2ImageData(patternID, palette, N);
        const canvasHolder = DrawImageData(imgData, 100, 100);
        canvasHolder.setAttribute("data-patternID", `${patternID}`);
        if (canvasHolder.firstChild) {
            canvasHolder.firstChild.onclick = (ev) => {
                if (!ev.ctrlKey) {
                    overlapPrinterData.printOverlaps(patternID, canvasHolder);
                }
                else {
                    ClearHTMLChildren(canvasHolder, true);
                }
            };
        }
    }
}
function Pattern2ImageData(patternID, palette, N) {
    const pattern = Pattern.FromPatternID(patternID, palette.length, N);
    const imgData = new ImageData(N, N);
    for (let i = 0; i < (N * N) * 4; i += 4) {
        const colorIndex = i / 4;
        imgData.data[i + 0] = palette[pattern.pixels[colorIndex]][0];
        imgData.data[i + 1] = palette[pattern.pixels[colorIndex]][1];
        imgData.data[i + 2] = palette[pattern.pixels[colorIndex]][2];
        imgData.data[i + 3] = palette[pattern.pixels[colorIndex]][3];
    }
    return imgData;
}
function DrawImageData(imgData, width, height) {
    const scratchcanvas = document.createElement("canvas");
    scratchcanvas.width = imgData.width;
    scratchcanvas.height = imgData.height;
    const scratchCtx = scratchcanvas.getContext("2d");
    scratchCtx.putImageData(imgData, 0, 0);
    const canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = height;
    canvas.setAttribute("style", "border: 5px solid purple; margin: 5px;");
    const ctx = canvas.getContext("2d");
    ctx.imageSmoothingEnabled = false;
    const canvasHolder = document.createElement("div");
    canvasHolder.appendChild(canvas);
    storageDiv.appendChild(canvasHolder);
    ctx.fillStyle = "#222";
    ctx.fillRect(0, 0, 1000, 1000);
    ctx.drawImage(scratchcanvas, 0, 0, imgData.width, imgData.height, 0, 0, width, height);
    return canvasHolder;
}
function createElement(type, innerText) {
    const element = document.createElement(type);
    element.innerText = innerText || "";
    //document.body.appendChild(element);
    storageDiv.appendChild(element);
    return element;
}
function DrawOverlapTable(patternID, overlapTable, patterns, palette, N, parentNode) {
    console.log("Drawing Overlaps");
    //New Line and Header
    parentNode.appendChild(document.createElement("br"));
    const header = document.createElement("h3");
    header.innerText = `Overlap Table for Pattern #${patternID}`;
    parentNode.appendChild(header);
    parentNode.appendChild(document.createElement("hr"));
    const startLoop = -N + 1, endLoop = N, iterations = endLoop - startLoop;
    for (let dy = startLoop; dy < endLoop; dy++) {
        const rowDiv = document.createElement("div");
        rowDiv.style.display = "inline-block";
        rowDiv.style.border = "5px solid black";
        //rowDiv.style.width = `${(iterations + 1) * 200 + 30}px`;
        //rowDiv.style.height = "210px";
        for (let dx = startLoop; dx < endLoop; dx++) {
            const cellDiv = document.createElement("div");
            cellDiv.style.display = "inline-block";
            cellDiv.style.border = "5px solid darkslategrey";
            cellDiv.style.width = "200px";
            cellDiv.style.height = "200px";
            cellDiv.style.cssFloat = "left";
            cellDiv.style.overflow = "auto";
            if (dx === 0 && dy === 0) {
                cellDiv.style.display = "flex";
                cellDiv.style.justifyContent = "center";
                cellDiv.style.alignItems = "center";
            }
            for (const pattern of overlapTable.GetAllOverlaps(dx, dy)) {
                const imgData = Pattern2ImageData(patterns[pattern], palette, N);
                const scale = (dx === 0 && dy === 0) ? 5 : 1;
                const canvas = DrawImageData(imgData, 25 * scale, 25 * scale);
                canvas.style.display = "inline-block";
                cellDiv.appendChild(canvas);
            }
            rowDiv.appendChild(cellDiv);
        }
        parentNode.appendChild(rowDiv);
        parentNode.appendChild(document.createElement("br"));
    }
    parentNode.appendChild(document.createElement("hr"));
    console.log("Drawing Overlaps Complete");
}
function ClearHTMLChildren(element, exceptFirst = false) {
    console.log(`Clearing child elements`);
    while (element.lastChild && element.lastChild !== element.firstChild) {
        element.removeChild(element.lastChild);
    }
    if (!exceptFirst && element.firstChild) {
        element.removeChild(element.firstChild);
    }
}
function DrawSolverState(solver, gridCellSize = 32, canvas) {
    if (!canvas) {
        canvas = document.createElement("canvas");
        canvas.style.border = "5px solid black";
    }
    const ctx = canvas.getContext("2d");
    const complete = solver.complete;
    canvas.width = solver.width * gridCellSize;
    canvas.height = solver.height * gridCellSize;
    storageDiv.insertBefore(canvas, storageDiv.childNodes[0]);
    ctx.fillStyle = "#008";
    ctx.fillRect(0, 0, 1000, 1000);
    ctx.save();
    //Text styling
    ctx.font = `${gridCellSize}px sans-serif`;
    ctx.textBaseline = "middle";
    ctx.textAlign = "center";
    ctx.fillStyle = "white";
    //Precalculate patterns
    const patterns = solver.patternIDs.map(id => Pattern.FromPatternID(id, solver.paletteSize, solver.N));
    for (let x = 0; x < solver.width; x++) {
        for (let y = 0; y < solver.height; y++) {
            //Get the current cell
            const cell = solver.resultGrid[x][y];
            //Count possible options in the cell
            const validPatternCount = solver.GetCellPatternCount(cell);
            //Finished Cell
            if (validPatternCount === 1 && complete) {
                const cellFinalPattern = solver.GetCellPattern(cell);
                const cellFinalColor = patterns[cellFinalPattern].pixels[0];
                const c = solver.palette[cellFinalColor];
                const cssColor = `rgb(${c[0]},${c[1]},${c[2]})`;
                ctx.fillStyle = cssColor;
                ctx.fillRect(x * gridCellSize, y * gridCellSize, gridCellSize, gridCellSize);
            }
            //Contradiction
            else if (validPatternCount === 0) {
                ctx.fillStyle = "#F00";
                ctx.fillRect(x * gridCellSize, y * gridCellSize, gridCellSize, gridCellSize);
                ctx.fillStyle = "#000";
                ctx.fillText('\u2612', x * gridCellSize + gridCellSize / 2, y * gridCellSize + gridCellSize / 2, gridCellSize);
            }
            //Cell in progress
            else {
                //Pick a background to show cell progress
                //const patternCount = solver.patternIDs.length;
                //const lerpFactor = (patternCount <= 1) ? 1 : 1 - ((validPatternCount - 1) / (patternCount - 1)); //Just inverse lerp
                //ctx.fillStyle = ColorLerp([255,200,240,255], [0,0,136,255], lerpFactor);
                ctx.fillStyle = GetAverageColor(cell, patterns, solver.palette);
                ctx.fillRect(x * gridCellSize, y * gridCellSize, gridCellSize, gridCellSize);
            }
            ctx.fillStyle = "#F0F"; //Reset fill style
            //Set alpha and draw numbers repeatedly
            if (!complete) {
                ctx.globalAlpha = 1 / validPatternCount;
                for (let i = 0; i < solver.patternIDs.length; i++) {
                    if (cell[i]) {
                        ctx.fillText(i.toString(), x * gridCellSize + gridCellSize / 2, y * gridCellSize + gridCellSize / 2, gridCellSize);
                    }
                }
                ctx.globalAlpha = 1;
            }
        }
    }
    ctx.restore();
    //Draw Cell Grid lines
    if (!complete) {
        ctx.strokeStyle = "#303";
        ctx.lineWidth = 3;
        for (let x = 0; x < solver.width; x++) {
            for (let y = 0; y < solver.height; y++) {
                ctx.strokeRect(x * gridCellSize, y * gridCellSize, gridCellSize, gridCellSize);
            }
        }
    }
    return canvas;
}
function ColorLerp(a, b, t) {
    const c = [
        a[0] * (1 - t) + b[0] * t,
        a[1] * (1 - t) + b[1] * t,
        a[2] * (1 - t) + b[2] * t,
    ];
    return `rgb(${c[0]}, ${c[1]}, ${c[2]})`;
}
function GetAverageColor(cell, patterns, palette) {
    const patternList = [];
    for (let i = 0; i < cell.length; i++) {
        if (cell[i])
            patternList.push(i);
    }
    const finalColor = [0, 0, 0];
    for (let i = 0; i < patternList.length; i++) {
        const colorIndex = patterns[patternList[i]].pixels[0];
        const color = palette[colorIndex];
        finalColor[0] += color[0];
        finalColor[1] += color[1];
        finalColor[2] += color[2];
    }
    finalColor[0] /= patternList.length;
    finalColor[1] /= patternList.length;
    finalColor[2] /= patternList.length;
    const cssColor = `rgb(${finalColor[0]},${finalColor[1]},${finalColor[2]})`;
    return cssColor;
}
function rndSeed() {
    const randomInt = Math.floor(Math.random() * 4294967296);
    seedInput.value = randomInt.toString();
    seed = randomInt;
}
function GeneratePatterns() {
    //External Inputs
    outputWidth = +outputWidthInput.value || 8;
    outputHeight = +outputHeightInput.value || 8;
    visualizerGridSize = +gridSizeInput.value || 32;
    globalN = +patternSizeInput.value || 2;
    //If N=1, the patterns returned is actually the color palette and the frequencies (times 8) of the colors! 
    const N = globalN;
    ClearHTMLChildren(storageDiv);
    //Show the source image
    DrawImageData(imgData, 200, 200);
    createElement("hr");
    //Generate Patterns
    const { palette, indexedImage } = PatternGenerator.PalettizeImage(imgData);
    const patternTable = PatternGenerator.GeneratePatterns(indexedImage, N, palette.length);
    //Show the generated patterns
    ShowPatterns(patternTable, palette, N, overlapPrinterData);
    //Generate Overlap Tables
    const patternIDs = [...patternTable.patternFrequencies.keys()];
    const overlapTables = PatternGenerator.GeneratePatternOverlapTable(patternIDs, N, palette.length);
    //Draw the overlaps
    overlapPrinterData.overlapTables = overlapTables;
    overlapPrinterData.patterns = patternIDs;
    overlapPrinterData.palette = palette;
    overlapPrinterData.N = N;
    //Setup Solver
    solver = new Solver(outputWidth, outputHeight, N, patternTable, palette, overlapTables, Math.random);
    //Setup Visualizer
    solverCanvas = DrawSolverState(solver, visualizerGridSize);
}
function LoadImage() {
    fileLoaded = false;
    console.log("Loading Image");
    const files = fileInput.files;
    if (files.length !== 0) {
        const file = files[0];
        const reader = new FileReader();
        reader.onload = function () {
            if (file.type.match('image.*')) {
                const sourceImage = new Image();
                sourceImage.onload = function () {
                    //OnVesselImageLoaded(sourceImage);
                    const canvas = document.createElement("canvas");
                    canvas.width = sourceImage.width;
                    canvas.height = sourceImage.height;
                    const ctx = canvas.getContext("2d");
                    ctx.drawImage(sourceImage, 0, 0);
                    imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                    console.log("Image Loaded Successfully");
                    fileLoaded = true;
                };
                sourceImage.src = this.result;
            }
        };
        reader.onerror = function () {
            console.error("Source Image failed to load!");
            fileLoaded = true;
        };
        reader.readAsDataURL(file);
    }
}
function RunSolver() {
    RunSolverProcedure(NoAnimationSolver);
}
function RunSolverAnimated() {
    RunSolverProcedure(AnimatedSolver);
}
function RunSolverProcedure(solverProcedure) {
    if (!solver)
        return;
    //Setup RNG
    //Contradiction seed: 1536909883
    seed = +seedInput.value || 123456789;
    seedRNG.Seed(seed);
    solver.rng = () => seedRNG.Random();
    solver.ClearCells();
    visualizerGridSize = +gridSizeInput.value || 32;
    //Solving procedure
    solverProcedure();
}
function AnimatedSolver() {
    clearInterval(interval);
    interval = setInterval(() => {
        const finished = solverIteration();
        DrawSolverState(solver, visualizerGridSize, solverCanvas);
        if (finished) {
            clearInterval(interval);
            DrawSolverState(solver, visualizerGridSize, solverCanvas);
        }
    }, 50);
}
function NoAnimationSolver() {
    DrawSolverState(solver, visualizerGridSize, solverCanvas);
    while (true) {
        const finished = solverIteration();
        if (finished) {
            DrawSolverState(solver, visualizerGridSize, solverCanvas);
            return true;
        }
    }
}
function solverIteration() {
    if (!solver)
        return true;
    try {
        const complete = solver.Observe();
        if (complete)
            return true;
        //DrawSolverState(solver, visualizerGridSize, solverCanvas);
        while (solver.Propagate()) {
            //DrawSolverState(solver, visualizerGridSize, solverCanvas);
        }
        //DrawSolverState(solver, visualizerGridSize, solverCanvas);
    }
    catch (e) {
        console.error(e);
        console.error(`If this is a contradiction: Seed: ${seed}, Size: (${solver.width}, ${solver.height}), N: ${solver.N}`);
        DrawSolverState(solver, visualizerGridSize, solverCanvas);
        return true;
    }
    return false;
}
/*
function ContradictionAnalyzer() {
    //contradictionStop = !contradictionStop;
    attempts = +attemptCounterInput.value || 10000;
    clearInterval(interval);

    const animatedInterval = () => {
        if (!solver) return;
        rndSeed();
        seed = +seedInput.value || 123456789;
        seedRNG.Seed(seed);
        solver.rng = () => seedRNG.Random();
        solver.ClearCells();
        interval = setInterval(() => {
            try {
                const finished = solverIterationNoCatch();
                if(finished){
                //if (finished || contradictionStop) {
                    clearInterval(interval);
                    //DrawSolverState(solver, true, visualizerGridSize, solverCanvas);
                    //if(!contradictionStop)
                    attemptCounterInput.value = attempts.toString();
                    if(attempts-- > 0) animatedInterval();
                }
            }
            catch (e) {
                console.error(e);
                console.error(`If this is a contradiction: seed is ${seed}`);
                DrawSolverState(solver, visualizerGridSize, solverCanvas);
                clearInterval(interval);
                //contradictionStop = false;
            }
        }, 0);
    };
    animatedInterval();
}

function solverIterationNoCatch() {
    if (!solver) return true;
    const complete = solver.Observe();
    if (complete) return true;
    //DrawSolverState(solver, false, solverCanvas);
    while (solver.Propagate()) {
        //DrawSolverState(solver, false, solverCanvas);
    }
    //DrawSolverState(solver, false, visualizerGridSize, solverCanvas);
    return false;
}
*/
//Source Image Data
let imgData = generateRedMaze();
//Stores generated elements
const storageDiv = document.getElementById("storage-div");
//The solver
let solver;
let solverCanvas;
let interval;
let seedRNG = new SeededRandom();
//let contradictionStop = true;
//let attempts = 10000;
//External Input
let outputWidthInput = document.getElementById("output-width");
let outputHeightInput = document.getElementById("output-height");
let gridSizeInput = document.getElementById("grid-size");
let patternSizeInput = document.getElementById("pattern-size");
let seedInput = document.getElementById("rng-seed");
let fileInput = document.getElementById("source-image-input");
//let attemptCounterInput = document.getElementById("attempt-counter") as HTMLInputElement;
fileInput.addEventListener("change", function () { LoadImage(); });
let fileLoaded = true;
let outputWidth = +outputWidthInput.value || 8;
let outputHeight = +outputHeightInput.value || 8;
let visualizerGridSize = +gridSizeInput.value || 32;
let globalN = +patternSizeInput.value || 2;
let seed = +seedInput.value || 123456789;
//Used for the overlap printer
const overlapPrinterData = {
    overlapTables: null,
    patterns: null,
    palette: null,
    N: 2,
    printOverlaps: function (patternID, parentElement) {
        const patterns = this.patterns;
        let patternIndex;
        for (patternIndex = 0; patternIndex < patterns.length; patternIndex++) {
            if (patterns[patternIndex] === patternID)
                break;
        }
        DrawOverlapTable(patternID, this.overlapTables[patternIndex], this.patterns, this.palette, this.N, parentElement);
    },
};
